const { GraphQLScalarType } = require('graphql')
const { Kind } = require('graphql/language')
const { ObjectID } = require('mongoose').mongo.ObjectID
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const createToken = (user, secret, expiresIn) => {
  const { username, email } = user
  return jwt.sign({ username, email }, secret, { expiresIn })
}

exports.resolvers = {
  ObjectID: new GraphQLScalarType({
    name: 'ObjectID',
    description:
      'The `ObjectID` scalar type represents a [`BSON`](https://en.wikipedia.org/wiki/BSON) ID commonly used in `mongodb`.',
    serialize(_id) {
      if (_id instanceof ObjectID) {
        return _id.toHexString()
      }
      if (typeof _id === 'string') {
        return _id
      }
      throw new Error(
        `${Object.getPrototypeOf(_id).constructor.name} not convertible to `
      )
    },
    parseValue(_id) {
      if (typeof _id === 'string') {
        return ObjectID.createFromHexString(_id)
      }
      throw new Error(`${typeof _id} not convertible to ObjectID`)
    },
    parseLiteral(ast) {
      if (ast.kind === Kind.STRING) {
        return ObjectID.createFromHexString(ast.value)
      }
      throw new Error(`${ast.kind} not convertible to ObjectID`)
    },
  }),

  Query: {
    getAllRecipes: async (root, args, { Recipe }) =>
      Recipe.find().sort({ createdAt: 'desc' }),

    getRecipe: async (root, { _id }, { Recipe }) => Recipe.findById(_id),

    searchRecipes: async (root, { searchTerm }, { Recipe }) => {
      if (searchTerm) {
        return Recipe.find(
          {
            $text: {
              $search: searchTerm,
              $caseSensitive: false,
              $diacriticSensitive: false,
            },
          },
          {
            score: { $meta: 'textScore' },
          }
        ).sort({
          score: { $meta: 'textScore', likes: 'desc', createdAt: 'desc' },
        })
      }
      return Recipe.find().sort({ likes: 'desc', createdAt: 'desc' })
    },

    getCurrentUser: async (root, args, { currentUser, User }) => {
      if (!currentUser) {
        return null
      }
      return User.findOne({
        username: currentUser.username,
      }).populate({
        path: 'favorites',
        model: 'Recipe',
      })
    },

    getUserRecipes: async (root, { username }, { Recipe }) =>
      Recipe.find({ username }).sort({ createdAt: 'desc' }),
  },

  Mutation: {
    addRecipe: async (
      root,
      { name, description, imageUrl, category, instructions, username },
      { Recipe }
    ) =>
      new Recipe({
        name,
        description,
        imageUrl,
        category,
        instructions,
        username,
      }).save(),

    deleteUserRecipe: async (root, { _id }, { Recipe }) =>
      Recipe.findByIdAndDelete(_id),

    updateUserRecipe: async (
      root,
      { _id, name, imageUrl, category, description, instructions },
      { Recipe }
    ) =>
      Recipe.findOneAndUpdate(
        { _id },
        { $set: { name, imageUrl, category, description, instructions } },
        { new: true }
      ),

    likeRecipe: async (root, { _id, username }, { Recipe, User }) => {
      const recipe = await Recipe.findByIdAndUpdate(_id, { $inc: { likes: 1 } })
      await User.findOneAndUpdate(
        { username },
        { $addToSet: { favorites: _id } }
      )
      return recipe
    },

    unlikeRecipe: async (root, { _id, username }, { Recipe, User }) => {
      const recipe = await Recipe.findByIdAndUpdate(_id, {
        $inc: { likes: -1 },
      })
      await User.findOneAndUpdate({ username }, { $pull: { favorites: _id } })
      return recipe
    },

    loginUser: async (root, { username, password }, { User }) => {
      const user = await User.findOne({ username })

      if (!user) {
        throw new Error('User not found.')
      }
      const isValidPassword = await bcrypt.compare(password, user.password)
      if (!isValidPassword) {
        throw new Error('Invalid password.')
      }
      return { token: createToken(user, process.env.SECRET, '1hr') }
    },

    registerUser: async (root, { username, email, password }, { User }) => {
      const user = await User.findOne({ username })

      if (user) {
        throw new Error('User already exists.')
      }
      const newUser = await new User({
        username,
        email,
        password,
      }).save()

      return { token: createToken(newUser, process.env.SECRET, '1hr') }
    },
  },
}

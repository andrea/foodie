const { gql } = require('apollo-server-express')

exports.typeDefs = gql`
  scalar ObjectID

  type Recipe {
    _id: ObjectID
    name: String!
    imageUrl: String!
    category: String!
    description: String!
    instructions: String!
    createdAt: String
    likes: Int
    username: String
  }

  type User {
    _id: ObjectID
    username: String!
    password: String!
    email: String!
    joinedAt: String
    favorites: [Recipe]
  }

  type Query {
    getAllRecipes: [Recipe]

    getRecipe(_id: ObjectID!): Recipe

    searchRecipes(searchTerm: String): [Recipe]

    getCurrentUser: User

    getUserRecipes(username: String!): [Recipe]
  }

  type Token {
    token: String!
  }

  type Mutation {
    addRecipe(
      name: String!
      description: String!
      imageUrl: String!
      category: String!
      instructions: String!
      username: String
    ): Recipe

    deleteUserRecipe(_id: ObjectID): Recipe

    updateUserRecipe(
      _id: ObjectID
      name: String!
      description: String!
      instructions: String!
      imageUrl: String!
      category: String!
      username: String
    ): Recipe

    likeRecipe(_id: ObjectID!, username: String!): Recipe

    unlikeRecipe(_id: ObjectID!, username: String!): Recipe

    loginUser(username: String!, password: String!): Token

    registerUser(username: String!, email: String!, password: String!): Token
  }
`

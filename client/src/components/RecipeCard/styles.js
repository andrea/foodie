export default theme => ({
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  avatar: {
    backgroundColor: theme.palette.secondary.main.light,
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
})

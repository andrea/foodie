import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
// prettier-ignore
import { Avatar, Card, CardContent, CardHeader, CardMedia, Grid, IconButton, Typography, withStyles } from '@material-ui/core'
import { Link as LinkIcon } from '@material-ui/icons'
import { textTruncate } from '../../utils/utils'
import styles from './styles'

class RecipeReviewCard extends React.Component {
  state = { expanded: false }

  render() {
    const { classes, recipe } = this.props

    return (
      <Grid item xs={12} sm={6} md={4} xl={3}>
        <Card className={classes.card}>
          <CardHeader
            avatar={
              <Avatar aria-label="Recipe" className={classes.avatar}>
                {recipe.category[0].toUpperCase()}
              </Avatar>
            }
            action={
              // eslint-disable-next-line no-underscore-dangle
              <IconButton component={Link} to={`/recipes/${recipe._id}`}>
                <LinkIcon />
              </IconButton>
            }
            title={recipe.name}
            subheader={recipe.category}
          />
          <CardMedia
            className={classes.media}
            image={recipe.imageUrl}
            title={`${recipe.name} presentation`}
          />
          <CardContent>
            <Typography component="p">
              {textTruncate(recipe.description)}
            </Typography>
          </CardContent>
        </Card>
      </Grid>
    )
  }
}

RecipeReviewCard.propTypes = {
  classes: PropTypes.object.isRequired,
  recipe: PropTypes.object,
}

export default withStyles(styles)(RecipeReviewCard)

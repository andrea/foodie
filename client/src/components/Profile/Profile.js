/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { compose } from 'recompose'
// prettier-ignore
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, Typography, withStyles } from '@material-ui/core'
import { ExpandMore } from '@material-ui/icons'
import withAuth from '../withAuth/withAuth'
import styles from './styles'
import { routes } from '../../config'
import UserFavorites from '../UserFavorites/UserFavorites'
import UserInfo from '../UserInfo/UserInfo'
import UserRecipes from '../UserRecipes/UserRecipes'

class Profile extends Component {
  state = {
    expanded: null,
  }

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    })
  }

  render() {
    const { expanded } = this.state
    const {
      classes,
      session: { getCurrentUser: user },
    } = this.props
    const { home } = routes

    if (!user) {
      return <Redirect to={home.path} />
    }

    return (
      <main className={classes.content}>
        <Typography variant="headline" align="center">
          Your Info
        </Typography>
        <div className={classes.root}>
          <ExpansionPanel defaultExpanded>
            <ExpansionPanelSummary expandIcon={<ExpandMore />}>
              <Typography className={classes.heading}>User</Typography>
              <Typography className={classes.secondaryHeading}>
                Account information
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.details}>
              <UserInfo user={user} />
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel
            expanded={expanded === 'panel2'}
            onChange={this.handleChange('panel2')}
          >
            <ExpansionPanelSummary expandIcon={<ExpandMore />}>
              <Typography className={classes.heading}>
                Your Favorites
              </Typography>
              <Typography className={classes.secondaryHeading}>
                Recipes you love
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <UserFavorites favorites={user.favorites} />
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel
            expanded={expanded === 'panel3'}
            onChange={this.handleChange('panel3')}
          >
            <ExpansionPanelSummary expandIcon={<ExpandMore />}>
              <Typography className={classes.heading}>Your Recipes</Typography>
              <Typography className={classes.secondaryHeading}>
                Amazing concoctions
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <UserRecipes username={user.username} />
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
      </main>
    )
  }
}

Profile.propTypes = {
  classes: PropTypes.object,
  session: PropTypes.object,
}

const isAuthenticated = session => session && session.getCurrentUser

export default compose(
  withStyles(styles),
  withAuth(isAuthenticated)
)(Profile)

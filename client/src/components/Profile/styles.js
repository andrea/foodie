export default theme => ({
  content: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    padding: `${theme.spacing.unit * 8}px 0`,
    [theme.breakpoints.down(
      theme.breakpoints.values.sm + theme.spacing.unit * 2 * 2
    )]: {
      padding: `${theme.spacing.unit * 2}px 0`,
    },
    [theme.breakpoints.up(
      theme.breakpoints.values.sm + theme.spacing.unit * 2 * 2
    )]: {
      width: theme.breakpoints.values.sm,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
    [theme.breakpoints.up(
      theme.breakpoints.values.sm + theme.spacing.unit * 3 * 2
    )]: {
      marginTop: theme.spacing.unit * 4,
      marginBottom: theme.spacing.unit * 8,
    },
  },
  heading: {
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    color: theme.palette.text.secondary,
  },
})

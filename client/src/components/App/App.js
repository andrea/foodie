import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
// prettier-ignore
import { BrowserRouter as Router, Redirect, Route, Switch, } from 'react-router-dom'
import WebFont from 'webfontloader'
// prettier-ignore
import { CssBaseline, createMuiTheme, MuiThemeProvider, } from '@material-ui/core'
import { defaultOptions, defaultPalette } from '../../theme/options'
import MenuAppBar from '../MenuAppBar/MenuAppBar'
import Sidebar from '../Sidebar/Sidebar'
import Login from '../Login/Login'
import Register from '../Register/Register'
import Profile from '../Profile/Profile'
import Recipes from '../Recipes/Recipes'
import AddRecipe from '../AddRecipe/AddRecipe'
import { routes } from '../../config'

WebFont.load({
  google: {
    families: ['Ubuntu:400,500,700', 'sans-serif'],
  },
})

class App extends Component {
  state = {
    drawer: {
      top: false,
      left: false,
    },
    paletteType: 'light',
  }

  componentDidMount() {
    if (localStorage && localStorage.getItem('foodie')) {
      const paletteType = JSON.parse(localStorage.getItem('foodie'))
      if (['light', 'dark'].includes(paletteType))
        this.setState(() => ({ paletteType }))
    }
  }

  toggleDrawer = (side, open) => () => {
    this.setState(prevState => ({
      drawer: { ...prevState.drawer, [side]: open },
    }))
  }

  togglePaletteType = () => {
    this.setState(
      prevState => ({
        paletteType: prevState.paletteType === 'light' ? 'dark' : 'light',
      }),
      () => {
        if (localStorage) {
          localStorage.setItem('foodie', JSON.stringify(this.state.paletteType))
        }
      }
    )
  }

  getTheme = theme =>
    createMuiTheme({
      ...defaultOptions,
      palette: {
        ...defaultPalette,
        type: theme.paletteType,
      },
    })

  toggleTheme = () =>
    this.getTheme({
      paletteType: this.state.paletteType,
    })

  render() {
    const { drawer } = this.state
    const { refetch, session } = this.props
    const { home, recipe, addRecipe, search, profile, login, register } = routes

    return (
      <MuiThemeProvider theme={this.toggleTheme()}>
        <CssBaseline />
        <Router>
          <Fragment>
            <MenuAppBar
              // toggleLeftDrawer={this.toggleLeftDrawer}
              toggleDrawer={this.toggleDrawer}
              togglePaletteType={this.togglePaletteType}
              paletteType={this.state.paletteType}
              session={session}
            />
            <Sidebar drawer={drawer} toggleDrawer={this.toggleDrawer} />
            <Switch>
              <Route
                path={home.path}
                exact
                render={() => <Recipes session={session} />}
              />
              <Route
                path={addRecipe.path}
                render={() => <AddRecipe session={session} />}
              />
              <Route path="/recipes/:_id" component={recipe.component} />
              <Route path={search.path} component={search.component} />
              <Route
                path={profile.path}
                render={() => <Profile session={session} />}
              />
              <Route
                path={login.path}
                render={() => <Login refetch={refetch} />}
              />
              <Route
                path={register.path}
                render={() => <Register refetch={refetch} />}
              />
              <Redirect to={home.path} />
            </Switch>
          </Fragment>
        </Router>
      </MuiThemeProvider>
    )
  }
}

App.propTypes = {
  refetch: PropTypes.func,
  session: PropTypes.object,
}

export default App

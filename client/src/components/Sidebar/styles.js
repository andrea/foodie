export default theme => ({
  title: {
    color: theme.palette.text.secondary,
    marginBottom: theme.spacing.unit / 2,
    '&:hover': {
      color: theme.palette.primary.main,
    },
  },

  appVersion: {
    color: theme.palette.text.secondary,
  },

  toolbarIe11: {
    display: 'flex',
  },

  toolbar: {
    ...theme.mixins.toolbar,
    backgroundColor: theme.palette.primary.dark,
    display: 'flex',
    flexGrow: 1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },

  list: {
    width: 250,
  },

  fullList: {
    width: 'auto',
  },

  active: {
    color: theme.palette.primary.main,
  },
})

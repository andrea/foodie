import React from 'react'
import { NavLink } from 'react-router-dom'
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import { Home, MoveToInbox, Search } from '@material-ui/icons'
import { routes } from '../../config'

const HomeLink = props => <NavLink to={routes.home.path} {...props} />
const AddRecipeLink = props => <NavLink to={routes.addRecipe.path} {...props} />
const SearchLink = props => <NavLink to={routes.search.path} {...props} />

export const groupOne = (
  <div>
    <ListItem button component={HomeLink}>
      <ListItemIcon>
        <Home />
      </ListItemIcon>
      <ListItemText primary="Home" />
    </ListItem>
    <ListItem button component={AddRecipeLink}>
      <ListItemIcon>
        <MoveToInbox />
      </ListItemIcon>
      <ListItemText primary="Add Recipe" />
    </ListItem>
    <ListItem button component={SearchLink}>
      <ListItemIcon>
        <Search />
      </ListItemIcon>
      <ListItemText primary="Search" />
    </ListItem>
  </div>
)

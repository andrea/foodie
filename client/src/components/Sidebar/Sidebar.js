import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
// prettier-ignore
import { Divider, List, SwipeableDrawer, withStyles, withTheme } from '@material-ui/core'
import withWidth, { isWidthUp } from '@material-ui/core/withWidth'
import Logo from '../Logo/Logo'
import { groupOne } from './menuItems'
import styles from './styles'

class Sidebar extends React.Component {
  render() {
    const { classes, drawer, theme, toggleDrawer } = this.props

    const sideList = (
      <div className={classes.list}>
        <div className={classes.toolbarIe11}>
          <div className={classes.toolbar}>
            <Logo
              fontSize={40}
              font={theme.typography.fontFamily}
              fill={theme.palette.common.white}
            />
          </div>
        </div>
        <Divider />
        <List>{groupOne}</List>
        <Divider />
      </div>
    )

    const fullList = (
      <div className={classes.fullList}>
        <List>{groupOne}</List>
        <Divider />
      </div>
    )

    if (isWidthUp('sm', this.props.width)) {
      return (
        <SwipeableDrawer
          open={drawer.left}
          onClose={toggleDrawer('left', false)}
          onOpen={toggleDrawer('left', true)}
        >
          <div
            tabIndex={0}
            role="button"
            onClick={toggleDrawer('left', false)}
            onKeyDown={toggleDrawer('left', false)}
          >
            {sideList}
          </div>
        </SwipeableDrawer>
      )
    }

    return (
      <SwipeableDrawer
        anchor="top"
        open={drawer.top}
        onClose={toggleDrawer('top', false)}
        onOpen={toggleDrawer('top', true)}
      >
        <div
          tabIndex={0}
          role="button"
          onClick={toggleDrawer('top', false)}
          onKeyDown={toggleDrawer('top', false)}
        >
          {fullList}
        </div>
      </SwipeableDrawer>
    )
  }
}

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired,
  drawer: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  toggleDrawer: PropTypes.func.isRequired,
  width: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl']),
}

export default compose(
  withWidth(),
  withStyles(styles),
  withTheme()
)(Sidebar)

import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { Redirect } from 'react-router-dom'
import { Mutation } from 'react-apollo'
// prettier-ignore
import { Button, FormControl, Input, InputLabel, Grid, MenuItem, Paper, Select, TextField, Typography, withStyles } from '@material-ui/core'
import withAuth from '../withAuth/withAuth'
import { addRecipe } from '../../graphql/mutations'
import Toast from '../Toast/Toast'
import { routes } from '../../config'
import { getAllRecipes, getUserRecipes } from '../../graphql/queries'
import styles from './styles'

const initialFormState = {
  name: '',
  category: '',
  imageUrl: '',
  description: '',
  instructions: '',
  username: '',
}

class AddRecipe extends Component {
  state = {
    form: { ...initialFormState },
    isSelectOpen: false,
    showToast: false,
    error: {},
  }

  componentDidMount() {
    const { getCurrentUser } = this.props.session
    this.setState(prevState => ({
      form: { ...prevState.form, username: getCurrentUser.username },
    }))
  }

  handleChange = event => {
    event.persist()
    this.setState(prevState => ({
      form: { ...prevState.form, [event.target.name]: event.target.value },
    }))
  }

  handleClose = () => {
    this.setState({ isSelectOpen: false })
  }

  handleOpen = () => {
    this.setState({ isSelectOpen: true })
  }

  handleSubmit = (event, addRecipeMutation) => {
    event.preventDefault()
    addRecipeMutation()
  }

  handleReset = () => {
    this.setState(() => ({ form: { ...initialFormState } }))
  }

  handleShowToast = error => {
    this.setState({ showToast: true, error })
  }

  handleHideToast = (event, reason) => {
    if (reason === 'clickaway') return
    this.setState({ showToast: false })
  }

  isValidForm = () => {
    // prettier-ignore
    const { form: { name, category, imageUrl, description, instructions } } = this.state
    return name && category && imageUrl && description && instructions
  }

  updateCache = (cache, { data: { addRecipe: recipe } }) => {
    // Read the recipes from our cache for this query.
    // More info => https://www.apollographql.com/docs/react/features/optimistic-ui.html#optimistic-advanced
    const { getAllRecipes: recipes } = cache.readQuery({
      query: getAllRecipes,
    })
    // Add our new recipe from the mutation to the beginning,
    // and write our recipes back to the cache.
    cache.writeQuery({
      query: getAllRecipes,
      data: {
        getAllRecipes: [recipe, ...recipes],
      },
    })
  }

  render() {
    const { form, isSelectOpen } = this.state
    const { classes } = this.props
    const { home } = routes

    return (
      <Mutation
        mutation={addRecipe}
        variables={{ ...form }}
        onError={this.handleShowToast}
        refetchQueries={() => [
          { query: getUserRecipes, variables: { username: form.username } },
        ]}
        update={this.updateCache}
      >
        {(addRecipeMutation, { data, loading }) => {
          if (data && !loading) {
            return <Redirect to={home.path} />
          }
          return (
            <Fragment>
              <main className={classes.content}>
                <Typography variant="headline" align="center">
                  Add New Recipe
                </Typography>
                <Paper className={classes.paper}>
                  <form
                    autoComplete="off"
                    onSubmit={event =>
                      this.handleSubmit(event, addRecipeMutation)
                    }
                    onReset={this.handleReset}
                  >
                    <Grid container spacing={24}>
                      <Grid item xs={12}>
                        <TextField
                          required
                          id="name"
                          name="name"
                          label="Recipe Name"
                          fullWidth
                          onChange={this.handleChange}
                          value={form.name}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <TextField
                          required
                          id="imageUrl"
                          name="imageUrl"
                          label="Recipe Image"
                          fullWidth
                          onChange={this.handleChange}
                          value={form.imageUrl}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <FormControl fullWidth required>
                          <InputLabel htmlFor="selectCategory">
                            Category
                          </InputLabel>
                          <Select
                            open={isSelectOpen}
                            onClose={this.handleClose}
                            onOpen={this.handleOpen}
                            value={this.state.form.category}
                            onChange={this.handleChange}
                            input={
                              <Input name="category" id="selectCategory" />
                            }
                          >
                            <MenuItem value="">
                              <em>None</em>
                            </MenuItem>
                            <MenuItem value="breakfast">Breakfast</MenuItem>
                            <MenuItem value="lunch">Lunch</MenuItem>
                            <MenuItem value="dinner">Dinner</MenuItem>
                            <MenuItem value="snack">Snack</MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                      <Grid item xs={12}>
                        <TextField
                          required
                          id="description"
                          name="description"
                          label="Recipe Description"
                          fullWidth
                          multiline
                          rows={5}
                          onChange={this.handleChange}
                          value={form.description}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <TextField
                          required
                          id="instructions"
                          name="instructions"
                          label="Recipe Instructions"
                          fullWidth
                          multiline
                          rows={8}
                          onChange={this.handleChange}
                          value={form.instructions}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid item xs={12} className={classes.buttons}>
                        <Button
                          variant="outlined"
                          size="large"
                          className={classes.button}
                          type="reset"
                          onClick={this.handleReset}
                        >
                          Reset Form
                        </Button>
                        <Button
                          disabled={loading || !this.isValidForm()}
                          variant="outlined"
                          size="large"
                          className={classes.button}
                          type="submit"
                        >
                          Add Recipe
                        </Button>
                      </Grid>
                    </Grid>
                  </form>
                </Paper>
                <Toast
                  close={this.handleHideToast}
                  message={this.state.error.message}
                  open={this.state.showToast}
                />
              </main>
            </Fragment>
          )
        }}
      </Mutation>
    )
  }
}

AddRecipe.propTypes = {
  classes: PropTypes.object,
  session: PropTypes.object,
}

const isAuthenticated = session => session && session.getCurrentUser

export default compose(
  withStyles(styles),
  withAuth(isAuthenticated)
)(AddRecipe)

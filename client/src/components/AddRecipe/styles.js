export default theme => ({
  appBar: {
    position: 'relative',
  },
  content: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    padding: `${theme.spacing.unit * 8}px 0`,
    [theme.breakpoints.down(
      theme.breakpoints.values.sm + theme.spacing.unit * 2 * 2
    )]: {
      padding: `${theme.spacing.unit * 2}px 0`,
    },
    [theme.breakpoints.up(
      theme.breakpoints.values.sm + theme.spacing.unit * 2 * 2
    )]: {
      width: theme.breakpoints.values.sm,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
    padding: theme.spacing.unit * 2,
    [theme.breakpoints.up(
      theme.breakpoints.values.sm + theme.spacing.unit * 3 * 2
    )]: {
      marginTop: theme.spacing.unit * 4,
      marginBottom: theme.spacing.unit * 8,
      padding: theme.spacing.unit * 3,
    },
  },
  instructions: {
    marginBottom: theme.spacing.unit,
  },
  buttons: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  button: {
    marginTop: theme.spacing.unit * 3,
  },
})

import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core'
import { Query } from 'react-apollo'
import { getRecipe } from '../../graphql/queries'
import ProgressBar from '../ProgressBar/ProgressBar'
import Toast from '../Toast/Toast'
import styles from './styles'
import RecipePost from '../RecipePost/RecipePost'

class RecipePage extends Component {
  state = {
    showToast: false,
    error: {},
  }

  handleShowToast = error => {
    this.setState({ showToast: true, error })
  }

  handleHideToast = (event, reason) => {
    if (reason === 'clickaway') return
    this.setState({ showToast: false })
  }

  render() {
    const { match, classes } = this.props
    const { _id } = match.params
    return (
      <Fragment>
        <Query
          query={getRecipe}
          variables={{ _id }}
          onError={this.handleShowToast}
        >
          {({ data, loading }) => {
            if (loading) return <ProgressBar show />
            return (
              <main className={classes.content}>
                <RecipePost recipe={data.getRecipe} />
              </main>
            )
          }}
        </Query>
        <Toast
          close={this.handleHideToast}
          message={this.state.error.message}
          open={this.state.showToast}
        />
      </Fragment>
    )
  }
}

RecipePage.propTypes = {
  match: PropTypes.object.isRequired,
  classes: PropTypes.object,
}

export default compose(
  withRouter,
  withStyles(styles)
)(RecipePage)

export default theme => ({
  root: {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    height: `calc(100vh - ${theme.mixins.toolbar})`,
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    padding: `${theme.spacing.unit * 3}px 0 ${theme.spacing.unit * 8}px`,
    [theme.breakpoints.up(
      theme.breakpoints.values.lg + theme.spacing.unit * 3 * 2
    )]: {
      width: theme.breakpoints.values.lg,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  mainFeaturedPost: {
    color: theme.palette.common.black,
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 4,
    position: 'relative',
    [theme.breakpoints.down(
      theme.breakpoints.values.md + theme.spacing.unit * 3 * 2
    )]: {
      marginLeft: 0,
      marginRight: 0,
      width: 'auto',
    },
    [theme.breakpoints.up(
      theme.breakpoints.values.lg + theme.spacing.unit * 3 * 2
    )]: {
      width: theme.breakpoints.values.lg,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  mainFeaturedPostContent: {
    padding: `${theme.spacing.unit * 6}px`,
    [theme.breakpoints.up('md')]: {
      paddingRight: 0,
    },
  },
  media: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%',
  },
})

import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Query } from 'react-apollo'
import { Grid, Typography, withStyles } from '@material-ui/core'
import Toast from '../Toast/Toast'
import RecipeCard from '../RecipeCard/RecipeCard'
import ProgressBar from '../ProgressBar/ProgressBar'
import { getAllRecipes } from '../../graphql/queries'
import styles from './styles'

class Recipes extends Component {
  state = {
    auth: false,
    showToast: false,
    error: {},
  }

  componentDidMount() {
    const { session } = this.props
    if (session && session.getCurrentUser) {
      this.setState(() => ({ auth: session.getCurrentUser }))
    }
  }

  // noinspection JSCheckFunctionSignatures
  componentDidUpdate(prevProps) {
    if (prevProps.session !== this.props.session) {
      this.setState(() => ({ auth: this.props.session.getCurrentUser }))
    }
  }

  handleShowToast = error => {
    this.setState({ showToast: true, error })
  }

  handleHideToast = (event, reason) => {
    if (reason === 'clickaway') return
    this.setState({ showToast: false })
  }

  render() {
    const { classes } = this.props

    return (
      <Fragment>
        <Query query={getAllRecipes} onError={this.handleShowToast}>
          {({ data, loading }) => {
            if (loading) return <ProgressBar show />
            return (
              <main className={classes.content}>
                <Typography variant="headline" gutterBottom align="center">
                  Eat Clean. Enjoy Life.
                </Typography>

                <Grid container spacing={40} className={classes.grid}>
                  {data.getAllRecipes.map(recipe => (
                    // eslint-disable-next-line no-underscore-dangle
                    <RecipeCard key={recipe._id} recipe={recipe} />
                  ))}
                </Grid>
                <Toast
                  close={this.handleHideToast}
                  message={this.state.error.message}
                  open={this.state.showToast}
                />
              </main>
            )
          }}
        </Query>
      </Fragment>
    )
  }
}

Recipes.propTypes = {
  classes: PropTypes.object.isRequired,
  session: PropTypes.object,
}

export default withStyles(styles)(Recipes)

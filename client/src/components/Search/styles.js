export default theme => ({
  root: {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    height: `calc(100vh - ${theme.mixins.toolbar})`,
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    padding: `${theme.spacing.unit * 8}px 0`,
    [theme.breakpoints.down(
      theme.breakpoints.values.sm + theme.spacing.unit * 2 * 2
    )]: {
      padding: `${theme.spacing.unit * 2}px 0`,
    },
    [theme.breakpoints.up(
      theme.breakpoints.values.lg + theme.spacing.unit * 3 * 2
    )]: {
      width: theme.breakpoints.values.lg,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  grid: {
    marginTop: theme.spacing.unit,
    [theme.breakpoints.down(
      theme.breakpoints.values.md + theme.spacing.unit * 2 * 2
    )]: {
      marginTop: 0,
    },
  },
  searchTerm: {
    backgroundColor:
      theme.palette.type === 'light'
        ? theme.palette.grey[200]
        : theme.palette.grey[800],
    margin: `${theme.spacing.unit * 3}px 0`,
    padding: `${theme.spacing.unit * 3}px`,
  },
  searchIcon: {
    color: theme.palette.grey[500],
  },
})

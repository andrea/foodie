import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { ApolloConsumer } from 'react-apollo'
// prettier-ignore
import { Grid, InputAdornment, TextField, Typography, withStyles, } from '@material-ui/core'
import { Restaurant } from '@material-ui/icons'
import { debounce } from 'lodash'
import ProgressBar from '../ProgressBar/ProgressBar'
import RecipeCard from '../RecipeCard/RecipeCard'
import Toast from '../Toast/Toast'
import { searchRecipes } from '../../graphql/queries'
import { getWelcomeText } from '../../utils/utils'

import styles from './styles'

class Search extends Component {
  state = {
    auth: false,
    showToast: false,
    searchTerm: '',
    searchResults: [],
    error: {},
  }

  handleChange = (event, client) => {
    event.persist()
    this.setState(() => ({ searchTerm: event.target.value }))
    this.handleDebouncedQuery(client)
  }

  handleDebouncedQuery = debounce(async client => {
    const { data } = await client.query({
      query: searchRecipes,
      variables: { searchTerm: this.state.searchTerm },
    })
    this.setState(() => ({ searchResults: data.searchRecipes }))
  }, 500)

  handleShowToast = error => {
    this.setState({ showToast: true, error })
  }

  handleHideToast = (event, reason) => {
    if (reason === 'clickaway') return
    this.setState({ showToast: false })
  }

  render() {
    const { searchTerm, searchResults } = this.state
    const { classes } = this.props
    return (
      <Fragment>
        <ApolloConsumer onError={this.handleShowToast}>
          {(client, loading) => {
            if (loading) return <ProgressBar show />
            return (
              <main className={classes.content}>
                <Typography variant="headline" gutterBottom align="center">
                  {getWelcomeText()}
                </Typography>
                <Fragment>
                  <form noValidate autoComplete="off">
                    <Grid container className={classes.searchTerm}>
                      <Grid item xs={12}>
                        <TextField
                          id="name"
                          name="name"
                          label="Find a recipe"
                          fullWidth
                          onChange={event => this.handleChange(event, client)}
                          value={searchTerm}
                          variant="outlined"
                          InputProps={{
                            startAdornment: (
                              <InputAdornment
                                position="start"
                                className={classes.searchIcon}
                              >
                                <Restaurant />
                              </InputAdornment>
                            ),
                          }}
                        />
                      </Grid>
                    </Grid>
                  </form>
                  <Grid container spacing={40} className={classes.grid}>
                    {searchResults.map(recipe => (
                      // eslint-disable-next-line no-underscore-dangle
                      <RecipeCard key={recipe._id} recipe={recipe} />
                    ))}
                  </Grid>
                </Fragment>
                <Toast
                  close={this.handleHideToast}
                  message={this.state.error.message}
                  open={this.state.showToast}
                />
              </main>
            )
          }}
        </ApolloConsumer>
      </Fragment>
    )
  }
}

Search.propTypes = {
  classes: PropTypes.object.isRequired,
  session: PropTypes.object,
}

export default withStyles(styles)(Search)

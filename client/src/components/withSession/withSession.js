import React from 'react'
import { Query } from 'react-apollo'
import { getCurrentUser } from '../../graphql/queries'

const withSession = Component =>
  function ComponentWithSession(props) {
    return (
      <Query query={getCurrentUser}>
        {({ data, loading, refetch }) => {
          if (loading) return null
          return <Component {...props} refetch={refetch} session={data} />
        }}
      </Query>
    )
  }

export default withSession

/* eslint-disable no-underscore-dangle */
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Mutation } from 'react-apollo'
// prettier-ignore
import { Button, Dialog, DialogContent, DialogTitle, FormControl, Grid, Input, InputLabel, MenuItem, Select, TextField, withStyles, } from '@material-ui/core'
import { updateUserRecipe } from '../../graphql/mutations'
import styles from './styles'

class EditRecipe extends Component {
  state = {
    isSelectOpen: false,
    recipe: {},
  }

  // noinspection JSCheckFunctionSignatures
  componentDidUpdate(prevProps) {
    // noinspection JSUnusedLocalSymbols
    const { __typename, createdAt, likes, ...recipe } = this.props.recipe
    if (prevProps.recipe !== this.props.recipe) {
      this.setState(() => ({ recipe: { ...recipe } }))
    }
  }

  handleChange = event => {
    event.persist()
    this.setState(prevState => ({
      recipe: { ...prevState.recipe, [event.target.name]: event.target.value },
    }))
  }

  handleSelectClose = () => {
    this.setState(() => ({ isSelectOpen: false }))
  }

  handleSelectOpen = () => {
    this.setState(() => ({ isSelectOpen: true }))
  }

  isValidForm = () => {
    // prettier-ignore
    const { recipe: { name, category, imageUrl, description, instructions } } = this.state
    return name && category && imageUrl && description && instructions
  }

  render() {
    const { isSelectOpen, recipe } = this.state
    // prettier-ignore
    const { classes, fullScreen, handleClose, handleOnError, handleUpdate, open } = this.props

    return (
      <Fragment>
        <Dialog
          fullScreen={fullScreen}
          open={open}
          onClose={() => handleClose('edit')}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{recipe.name}</DialogTitle>
          <DialogContent>
            <Mutation
              mutation={updateUserRecipe}
              variables={{ ...recipe }}
              onError={handleOnError}
            >
              {updateUserRecipeMutation => (
                <form
                  autoComplete="off"
                  onSubmit={event =>
                    handleUpdate(event, updateUserRecipeMutation)
                  }
                  onReset={this.handleReset}
                  className={classes.form}
                >
                  <Grid container spacing={24}>
                    <Grid item xs={12}>
                      <TextField
                        required
                        id="name"
                        name="name"
                        label="Recipe Name"
                        fullWidth
                        onChange={this.handleChange}
                        variant="outlined"
                        value={recipe.name}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        required
                        id="imageUrl"
                        name="imageUrl"
                        label="Recipe Image"
                        fullWidth
                        onChange={this.handleChange}
                        variant="outlined"
                        value={recipe.imageUrl}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <FormControl fullWidth required>
                        <InputLabel htmlFor="selectCategory">
                          Category
                        </InputLabel>
                        <Select
                          open={isSelectOpen}
                          onClose={this.handleSelectClose}
                          onOpen={this.handleSelectOpen}
                          onChange={this.handleChange}
                          input={<Input name="category" id="selectCategory" />}
                          value={recipe.category}
                        >
                          <MenuItem value="">
                            <em>None</em>
                          </MenuItem>
                          <MenuItem value="breakfast">Breakfast</MenuItem>
                          <MenuItem value="lunch">Lunch</MenuItem>
                          <MenuItem value="dinner">Dinner</MenuItem>
                          <MenuItem value="snack">Snack</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        required
                        id="description"
                        name="description"
                        label="Recipe Description"
                        fullWidth
                        multiline
                        placeholder="Recipe Description"
                        rows={5}
                        onChange={this.handleChange}
                        variant="outlined"
                        value={recipe.description}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        required
                        id="instructions"
                        name="instructions"
                        label="Recipe Instructions"
                        fullWidth
                        multiline
                        placeholder="Recipe Instructions"
                        rows={8}
                        onChange={this.handleChange}
                        variant="outlined"
                        value={recipe.instructions}
                      />
                    </Grid>
                    <Grid item xs={12} className={classes.buttons}>
                      <Button
                        variant="outlined"
                        size="large"
                        className={classes.button}
                        type="reset"
                        onClick={() => handleClose('edit')}
                      >
                        Cancel
                      </Button>
                      <Button
                        disabled={!this.isValidForm()}
                        variant="outlined"
                        size="large"
                        className={classes.button}
                        type="submit"
                      >
                        Update Recipe
                      </Button>
                    </Grid>
                  </Grid>
                </form>
              )}
            </Mutation>
          </DialogContent>
        </Dialog>
      </Fragment>
    )
  }
}

EditRecipe.propTypes = {
  classes: PropTypes.object,
  fullScreen: PropTypes.bool,
  open: PropTypes.bool,
  handleUpdate: PropTypes.func,
  handleOnError: PropTypes.func,
  handleClose: PropTypes.func,
  recipe: PropTypes.object,
  username: PropTypes.string,
}

export default withStyles(styles)(EditRecipe)

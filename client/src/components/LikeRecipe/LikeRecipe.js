/* eslint-disable no-underscore-dangle */
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { Mutation } from 'react-apollo'
import { Button, withStyles } from '@material-ui/core'
import withSession from '../withSession/withSession'
import Toast from '../Toast/Toast'
import { likeRecipe, unlikeRecipe } from '../../graphql/mutations'
import { getRecipe } from '../../graphql/queries'
import styles from './styles'

class LikeRecipe extends Component {
  state = {
    username: '',
    liked: false,
    showToast: false,
    error: {},
  }

  componentDidMount() {
    if (this.props.session.getCurrentUser) {
      const {
        _id,
        session: {
          getCurrentUser: { username, favorites },
        },
      } = this.props
      const prevLiked =
        favorites.findIndex(favorite => favorite._id === _id) > -1
      this.setState(() => ({ username, liked: prevLiked }))
    }
  }

  handleShowToast = error => {
    this.setState({ showToast: true, error })
  }

  handleHideToast = (event, reason) => {
    if (reason === 'clickaway') return
    this.setState({ showToast: false })
  }

  handleClick = (likeRecipeMutation, unlikeRecipeMutation) => {
    this.setState(
      prevState => ({ liked: !prevState.liked }),
      () => {
        this.handleLike(likeRecipeMutation, unlikeRecipeMutation)
      }
    )
  }

  handleLike = (likeRecipeMutation, unlikeRecipeMutation) => {
    if (this.state.liked) {
      likeRecipeMutation().then(async () => {
        await this.props.refetch()
      })
    } else {
      unlikeRecipeMutation().then(async () => {
        await this.props.refetch()
      })
    }
  }

  updateLike = (
    cache,
    {
      data: {
        likeRecipe: { likes },
      },
    }
  ) => {
    const { _id } = this.props
    const { getRecipe: recipe } = cache.readQuery({
      query: getRecipe,
      variables: { _id },
    })
    cache.writeQuery({
      query: getRecipe,
      variables: { _id },
      data: { getRecipe: { ...recipe, likes: likes + 1 } },
    })
  }

  updateUnlike = (
    cache,
    {
      data: {
        unlikeRecipe: { likes },
      },
    }
  ) => {
    const { _id } = this.props
    const { getRecipe: recipe } = cache.readQuery({
      query: getRecipe,
      variables: { _id },
    })
    cache.writeQuery({
      query: getRecipe,
      variables: { _id },
      data: { getRecipe: { ...recipe, likes: likes - 1 } },
    })
  }

  render() {
    const { username, liked } = this.state
    const { _id, classes } = this.props

    if (!username) {
      return null
    }

    return (
      <Fragment>
        <Mutation
          mutation={unlikeRecipe}
          variables={{ _id, username }}
          update={this.updateUnlike}
          onError={this.handleShowToast}
        >
          {unlikeRecipeMutation => (
            <Mutation
              mutation={likeRecipe}
              variables={{ _id, username }}
              update={this.updateLike}
              onError={this.handleShowToast}
            >
              {likeRecipeMutation => (
                <Button
                  onClick={() =>
                    this.handleClick(likeRecipeMutation, unlikeRecipeMutation)
                  }
                  size="medium"
                  color="secondary"
                  className={classes.action}
                >
                  {liked ? 'Unlike This Recipe' : 'Like This Recipe'}
                </Button>
              )}
            </Mutation>
          )}
        </Mutation>
        <Toast
          close={this.handleHideToast}
          message={this.state.error.message}
          open={this.state.showToast}
        />
      </Fragment>
    )
  }
}

LikeRecipe.propTypes = {
  _id: PropTypes.string,
  classes: PropTypes.object,
  session: PropTypes.object,
  refetch: PropTypes.func,
}

export default compose(
  withStyles(styles),
  withSession
)(LikeRecipe)

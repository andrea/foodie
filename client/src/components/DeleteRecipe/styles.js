export default theme => ({
  buttons: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  button: {
    margin: theme.spacing.unit,
  },
})

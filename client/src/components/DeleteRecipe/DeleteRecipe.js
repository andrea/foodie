/* eslint-disable no-underscore-dangle */
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
// prettier-ignore
import { Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, withStyles } from '@material-ui/core'
import { Mutation } from 'react-apollo'
import { deleteUserRecipe } from '../../graphql/mutations'
// prettier-ignore
import { getAllRecipes, getCurrentUser, getUserRecipes, } from '../../graphql/queries'
import styles from './styles'

class DeleteRecipe extends Component {
  render() {
    // prettier-ignore
    const { classes, fullScreen, recipe, username, handleDelete, handleClose, open, handleOnError } = this.props

    return (
      <Fragment>
        <Dialog
          fullScreen={fullScreen}
          open={open}
          onClose={() => handleClose('delete')}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{recipe.name}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Are you sure you want to delete this recipe?
            </DialogContentText>
          </DialogContent>
          <DialogActions className={classes.buttons}>
            <Button
              onClick={() => handleClose('delete')}
              autoFocus
              className={classes.button}
              size="large"
              variant="outlined"
            >
              Nope
            </Button>
            <Mutation
              mutation={deleteUserRecipe}
              variables={{ _id: recipe._id }}
              onError={handleOnError}
              refetchQueries={() => [
                { query: getAllRecipes },
                { query: getCurrentUser },
              ]}
              update={(
                cache,
                { data: { deleteUserRecipe: deletedRecipe } }
              ) => {
                const { getUserRecipes: recipes } = cache.readQuery({
                  query: getUserRecipes,
                  variables: { username },
                })
                cache.writeQuery({
                  query: getUserRecipes,
                  variables: { username },
                  data: {
                    getUserRecipes: recipes.filter(
                      r => r._id !== deletedRecipe._id
                    ),
                  },
                })
              }}
            >
              {(deleteUserRecipeMutation, attrs = {}) => (
                <Button
                  onClick={() => handleDelete(deleteUserRecipeMutation)}
                  className={classes.button}
                  size="large"
                  variant="outlined"
                >
                  {attrs.loading ? <CircularProgress /> : 'Yes, sure'}
                </Button>
              )}
            </Mutation>
          </DialogActions>
        </Dialog>
      </Fragment>
    )
  }
}

DeleteRecipe.propTypes = {
  classes: PropTypes.object,
  fullScreen: PropTypes.bool,
  recipe: PropTypes.object,
  username: PropTypes.string,
  open: PropTypes.bool,
  handleDelete: PropTypes.func,
  handleClose: PropTypes.func,
  handleOnError: PropTypes.func,
}

export default withStyles(styles)(DeleteRecipe)

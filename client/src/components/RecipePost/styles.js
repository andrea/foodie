export default theme => ({
  content: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(
      theme.breakpoints.values.lg + theme.spacing.unit * 3 * 2
    )]: {
      width: theme.breakpoints.values.lg,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  mainFeaturedPost: {
    backgroundColor: theme.palette.grey[800],
    color: theme.palette.common.white,
    marginBottom: theme.spacing.unit * 4,
  },
  mainFeaturedPostContent: {
    padding: `${theme.spacing.unit * 6}px`,
    paddingRight: 0,
    [theme.breakpoints.up('md')]: {
      paddingRight: 0,
      paddingTop: '23.6%', // 1st Fibonacci arc
    },
    [theme.breakpoints.down('md')]: {
      paddingRight: 0,
      paddingTop: '38.2%', // 2nd Fibonacci arc
    },
    [theme.breakpoints.down('sm')]: {
      paddingRight: 0,
      paddingTop: '50%', // 3rd Fibonacci arc
    },
    [theme.breakpoints.down('xs')]: {
      paddingRight: 0,
      paddingTop: '61.8%', // 4th Fibonacci arc
    },
  },
  markdown: {
    padding: `${theme.spacing.unit * 3}px 0`,
  },
  mainGrid: {
    marginTop: theme.spacing.unit * 3,
  },
  textContent: {
    padding: `${theme.spacing.unit * 3}px 0`,
  },
  sidebarSection: {
    marginTop: theme.spacing.unit * 3,
  },
  avatar: {
    backgroundColor: theme.palette.secondary.main,
  },
  action: {
    marginBottom: theme.spacing.unit,
  },
})

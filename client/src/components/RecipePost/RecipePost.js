/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
// prettier-ignore
import { Avatar, Card, CardHeader, CardContent, CardActions, CardMedia, Divider, Grid, Typography, withStyles } from '@material-ui/core'
import { Restaurant } from '@material-ui/icons'
import Markdown from '../Markdown/Markdown'
import LikeRecipe from '../LikeRecipe/LikeRecipe'
import styles from './styles'

class Blog extends Component {
  render() {
    const { classes, recipe } = this.props

    return (
      <React.Fragment>
        <Card className={classes.mainFeaturedPost}>
          <CardMedia
            className={classes.mainFeaturedPostContent}
            image={recipe.imageUrl}
            title="Contemplative Reptile"
          />
        </Card>
        <Grid container spacing={40} className={classes.mainGrid}>
          <Grid item xs={12} md={8}>
            <Typography variant="body2" gutterBottom color="textSecondary">
              Category: {recipe.category}
            </Typography>
            <Divider />
            <div className={classes.textContent}>
              <Typography variant="headline" gutterBottom color="textPrimary">
                {recipe.name}
              </Typography>
              <Typography variant="caption" gutterBottom>
                {new Date(Date(recipe.createdAt)).toDateString()}
              </Typography>
              <Markdown className={classes.markdown}>
                {recipe.description}
              </Markdown>
            </div>
            <Divider className={classes.divider} />
            <div className={classes.textContent}>
              <Typography variant="title" gutterBottom color="textSecondary">
                Instructions
              </Typography>
              <Markdown className={classes.markdown}>
                {recipe.instructions}
              </Markdown>
            </div>
          </Grid>
          <Grid item xs={12} md={4}>
            <Card elevation={0}>
              <CardHeader
                avatar={
                  <Avatar aria-label="Recipe" className={classes.avatar}>
                    <Restaurant />
                  </Avatar>
                }
                title={`Created by: ${recipe.username}`}
              />
              <CardContent>
                <Typography>
                  {recipe.likes > 0
                    ? `${recipe.likes} people like this recipe.`
                    : 'No likes so far. Be the first!'}
                </Typography>
              </CardContent>
              <CardActions>
                <LikeRecipe _id={recipe._id} />
              </CardActions>
            </Card>
          </Grid>
        </Grid>
      </React.Fragment>
    )
  }
}

Blog.propTypes = {
  classes: PropTypes.object.isRequired,
  recipe: PropTypes.object.isRequired,
}

export default withStyles(styles)(Blog)

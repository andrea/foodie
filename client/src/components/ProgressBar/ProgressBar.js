import React from 'react'
import PropTypes from 'prop-types'
import { Fade, LinearProgress, withStyles } from '@material-ui/core'
import styles from './styles'

class LinearDeterminate extends React.Component {
  timer = null

  state = {
    completed: 0,
  }

  componentDidMount() {
    this.timer = setInterval(this.progress, 500)
  }

  componentWillUnmount() {
    clearInterval(this.timer)
  }

  progress = () => {
    const { completed } = this.state
    if (completed === 100) {
      this.setState({ completed: 0 })
    } else {
      const diff = Math.random() * 10
      this.setState({ completed: Math.min(completed + diff, 100) })
    }
  }

  render() {
    const { classes, show } = this.props
    return (
      <div className={classes.root}>
        <Fade in={show}>
          <LinearProgress
            variant="determinate"
            color="secondary"
            value={this.state.completed}
          />
        </Fade>
      </div>
    )
  }
}

LinearDeterminate.propTypes = {
  classes: PropTypes.object.isRequired,
  show: PropTypes.bool.isRequired,
}

LinearDeterminate.defaultProps = {
  show: false,
}

export default withStyles(styles)(LinearDeterminate)

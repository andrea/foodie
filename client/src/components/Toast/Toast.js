/* eslint-disable security/detect-object-injection */
import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// prettier-ignore
import { IconButton, Snackbar, SnackbarContent, withStyles, } from '@material-ui/core'
// prettier-ignore
import { CheckCircle as CheckCircleIcon, Close as CloseIcon, Error as ErrorIcon, Info as InfoIcon, Warning as WarningIcon, } from '@material-ui/icons'
import { amber, green } from '@material-ui/core/colors'

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
}

const styles1 = theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
})

const ToastContent = props => {
  const { classes, className, message, onClose, variant, ...other } = props
  const Icon = variantIcon[variant]

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  )
}

ToastContent.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  message: PropTypes.node,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
}

const ToastContentWrapper = withStyles(styles1)(ToastContent)

const styles2 = theme => ({
  margin: {
    margin: theme.spacing.unit,
  },
})

const Toast = props => {
  const { classes, close, horizontal, message, open, variant, vertical } = props

  return (
    <Snackbar
      anchorOrigin={{ vertical, horizontal }}
      open={open}
      autoHideDuration={6000}
      onClose={close}
    >
      <ToastContentWrapper
        className={classes.margin}
        onClose={close}
        variant={variant}
        message={message}
      />
    </Snackbar>
  )
}

Toast.propTypes = {
  classes: PropTypes.object.isRequired,
  close: PropTypes.func.isRequired,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
  message: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  vertical: PropTypes.string.isRequired,
  horizontal: PropTypes.string.isRequired,
}

Toast.defaultProps = {
  variant: 'error',
  message: 'An error occurred.',
  vertical: 'bottom',
  horizontal: 'center',
}

export default withStyles(styles2)(Toast)

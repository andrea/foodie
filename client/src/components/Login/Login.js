import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Link, Redirect } from 'react-router-dom'
import { Mutation } from 'react-apollo'
// prettier-ignore
import { Avatar, Button, FormControl, Paper, TextField, Typography, withStyles } from '@material-ui/core'
import { LockOutlined } from '@material-ui/icons'
import { loginUser } from '../../graphql/mutations'
import styles from './styles'
import Toast from '../Toast/Toast'
import { routes } from '../../config'

const initialUserState = {
  username: '',
  password: '',
}

class Login extends Component {
  state = {
    user: { ...initialUserState },
    showToast: false,
    error: {},
  }

  handleChange = e => {
    e.persist()
    this.setState(prevState => ({
      user: { ...prevState.user, [e.target.name]: e.target.value },
    }))
  }

  handleSubmit = (event, loginUserMutation) => {
    event.preventDefault()
    loginUserMutation()
      .then(async ({ data }) => {
        localStorage.setItem('token', data.loginUser.token)
        await this.props.refetch()
      })
      .catch(() => {})
  }

  handleShowToast = error => {
    this.setState({ showToast: true, error })
  }

  handleHideToast = (event, reason) => {
    if (reason === 'clickaway') return
    this.setState({ showToast: false })
  }

  isValidForm = () => {
    // prettier-ignore
    const { user: { username, password }, } = this.state
    return username && password
  }

  render() {
    // prettier-ignore
    const { user: { username, password }, } = this.state
    const { classes } = this.props
    const { home, register } = routes

    return (
      <Fragment>
        <main className={classes.layout}>
          <Paper className={classes.paper} elevation={6}>
            <Avatar className={classes.avatar}>
              <LockOutlined className={classes.icon} />
            </Avatar>
            <Typography variant="headline">Welcome back!</Typography>
            <Mutation
              mutation={loginUser}
              variables={{ username, password }}
              onError={this.handleShowToast}
            >
              {(loginUserMutation, { data, loading }) => {
                if (data) {
                  return <Redirect to={home.path} />
                }

                return (
                  <form
                    className={classes.form}
                    onSubmit={event =>
                      this.handleSubmit(event, loginUserMutation)
                    }
                  >
                    <FormControl margin="normal" required fullWidth>
                      <TextField
                        id="username"
                        name="username"
                        label="Username"
                        fullWidth
                        autoComplete="username"
                        onChange={this.handleChange}
                        value={username}
                        variant="outlined"
                      />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                      <TextField
                        name="password"
                        label="Password"
                        fullWidth
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        onChange={this.handleChange}
                        value={password}
                        variant="outlined"
                      />
                    </FormControl>
                    <Button
                      type="submit"
                      disabled={loading || !this.isValidForm()}
                      fullWidth
                      size="large"
                      variant="outlined"
                      className={classes.submit}
                    >
                      Login
                    </Button>
                  </form>
                )
              }}
            </Mutation>
          </Paper>
          <Typography
            variant="body1"
            align="center"
            className={classes.appendix}
          >
            No account yet? <Link to={register.path}>Create one</Link>.
          </Typography>
          <Toast
            close={this.handleHideToast}
            message={this.state.error.message}
            open={this.state.showToast}
          />
        </main>
      </Fragment>
    )
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
  refetch: PropTypes.func,
}

export default withStyles(styles)(Login)

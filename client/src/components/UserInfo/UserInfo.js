import React from 'react'
import PropTypes from 'prop-types'
import { TextField, withStyles } from '@material-ui/core'
import styles from './styles'
import { formatDate } from '../../utils/utils'

const UserInfo = ({ user, classes }) => (
  <form noValidate autoComplete="off" className={classes.form}>
    <TextField
      disabled
      fullWidth
      id="outlined-disabled"
      label="Username"
      defaultValue={user.username}
      margin="normal"
      variant="outlined"
    />
    <TextField
      disabled
      fullWidth
      id="outlined-disabled"
      label="Email"
      defaultValue={user.email}
      margin="normal"
      variant="outlined"
    />
    <TextField
      disabled
      fullWidth
      id="outlined-disabled"
      label="Join Date"
      defaultValue={formatDate(user.joinedAt)}
      margin="normal"
      variant="outlined"
    />
  </form>
)

UserInfo.propTypes = {
  classes: PropTypes.object,
  user: PropTypes.object,
}

export default withStyles(styles)(UserInfo)

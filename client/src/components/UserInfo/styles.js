export default () => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
})

/* eslint-disable no-underscore-dangle */
import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Button, withStyles } from '@material-ui/core'
import styles from './styles'

const UserFavorites = ({ favorites, classes }) => {
  if (favorites.length < 1) {
    return (
      <Button
        disabled
        variant="outlined"
        size="large"
        fullWidth
        className={classes.button}
      >
        You currently have no favorites. Go add some!
      </Button>
    )
  }
  return (
    <div className={classes.content}>
      {favorites.map(recipe => (
        <Button
          key={recipe._id}
          component={Link}
          to={`/recipes/${recipe._id}`}
          variant="outlined"
          size="large"
          className={classes.button}
          color="secondary"
        >
          {recipe.name}
        </Button>
      ))}
    </div>
  )
}

UserFavorites.propTypes = {
  favorites: PropTypes.arrayOf(PropTypes.object),
  classes: PropTypes.object,
}

export default withStyles(styles)(UserFavorites)

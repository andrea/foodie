export default theme => ({
  content: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
  },
  button: {
    textTransform: 'none',
    margin: theme.spacing.unit,
  },
})

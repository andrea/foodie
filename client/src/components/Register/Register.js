import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Link, Redirect } from 'react-router-dom'
import { Mutation } from 'react-apollo'
// prettier-ignore
import { Avatar, Button, FormControl, Paper, TextField, Typography, withStyles } from '@material-ui/core'
import { LockOutlined } from '@material-ui/icons'
import { registerUser } from '../../graphql/mutations'
import styles from './styles'
import Toast from '../Toast/Toast'
import { routes } from '../../config'

const initialUserState = {
  username: '',
  email: '',
  password: '',
  password2: '',
}

class Register extends Component {
  state = {
    user: { ...initialUserState },
    showToast: false,
    error: {},
  }

  handleChange = e => {
    e.persist()
    this.setState(prevState => ({
      user: { ...prevState.user, [e.target.name]: e.target.value },
    }))
  }

  handleSubmit = (event, registerUserMutation) => {
    event.preventDefault()
    registerUserMutation()
      .then(async ({ data }) => {
        localStorage.setItem('token', data.registerUser.token)
        await this.props.refetch()
      })
      .catch(() => {})
  }

  handleShowToast = error => {
    this.setState({ showToast: true, error })
  }

  handleHideToast = (event, reason) => {
    if (reason === 'clickaway') return
    this.setState({ showToast: false })
  }

  isValidForm = () => {
    // prettier-ignore
    const { user: { username, email, password, password2 } } = this.state
    return username && email && password && password === password2
  }

  render() {
    // prettier-ignore
    const { user: { username, email, password, password2 } } = this.state
    const { classes } = this.props
    const { home, login } = routes

    return (
      <Fragment>
        <main className={classes.layout}>
          <Paper className={classes.paper} elevation={6}>
            <Avatar className={classes.avatar}>
              <LockOutlined className={classes.icon} />
            </Avatar>
            <Typography variant="headline">Create an Account</Typography>
            <Mutation
              mutation={registerUser}
              variables={{ username, email, password }}
              onError={this.handleShowToast}
            >
              {(registerUserMutation, { data, loading }) => {
                if (data) {
                  return <Redirect to={home.path} />
                }

                return (
                  <form
                    className={classes.form}
                    onSubmit={event =>
                      this.handleSubmit(event, registerUserMutation)
                    }
                  >
                    <FormControl margin="normal" required fullWidth>
                      <TextField
                        id="username"
                        name="username"
                        label="Username"
                        fullWidth
                        autoComplete="username"
                        onChange={this.handleChange}
                        value={username}
                        variant="outlined"
                      />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                      <TextField
                        id="email"
                        name="email"
                        label="Email Address"
                        fullWidth
                        autoComplete="email"
                        onChange={this.handleChange}
                        value={email}
                        variant="outlined"
                      />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                      <TextField
                        name="password"
                        label="Password"
                        fullWidth
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        onChange={this.handleChange}
                        value={password}
                        variant="outlined"
                      />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                      <TextField
                        name="password2"
                        label="Confirm Password"
                        type="password"
                        id="password2"
                        autoComplete="current-password"
                        onChange={this.handleChange}
                        value={password2}
                        variant="outlined"
                      />
                    </FormControl>
                    <Button
                      type="submit"
                      disabled={loading || !this.isValidForm()}
                      fullWidth
                      size="large"
                      variant="outlined"
                      className={classes.submit}
                    >
                      Register
                    </Button>
                  </form>
                )
              }}
            </Mutation>
          </Paper>
          <Typography
            variant="body1"
            align="center"
            className={classes.appendix}
          >
            Already have an account? <Link to={login.path}>Login here</Link>.
          </Typography>
          <Toast
            close={this.handleHideToast}
            message={this.state.error.message}
            open={this.state.showToast}
          />
        </main>
      </Fragment>
    )
  }
}

Register.propTypes = {
  classes: PropTypes.object.isRequired,
  refetch: PropTypes.func,
}

export default withStyles(styles)(Register)

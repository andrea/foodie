export default theme => ({
  layout: {
    width: 'auto',
    display: 'block', // Fix IE11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 8,
    // Set media query at minimum width of 400,
    // plus some additional space derived from theme spacing settings,
    // coherent with the design and the 8px grid. => https://material-ui.com/layout/breakpoints/#breakpoints
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`,
  },
  avatar: {
    width: 64,
    height: 64,
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.primary.main,
  },
  icon: {
    fontSize: 48,
  },
  form: {
    width: '100%', // Fix IE11 issue.
    marginTop: theme.spacing.unit * 3,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  appendix: {
    marginTop: theme.spacing.unit * 3,
    '& a:link, a:visited': {
      color: theme.palette.primary.main,
    },
  },
})

export default theme => ({
  footer: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    position: 'relative',
    bottom: 0,
    backgroundColor: theme.palette.background.paper,
    padding: `${theme.spacing.unit * 6}px 0`,
  },
})

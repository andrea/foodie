import React from 'react'
import PropTypes from 'prop-types'
import { Typography, withStyles } from '@material-ui/core'
import styles from './styles'
import Emoji from '../Emoji/Emoji'

const Footer = ({ classes }) => (
  <footer className={classes.footer}>
    <Typography variant="body1" align="center" gutterBottom>
      We <Emoji label="Red Hearth" symbol="❤️" />
      Cooking!
    </Typography>
    <Typography
      variant="caption"
      align="center"
      color="textSecondary"
      component="p"
    >
      ...and won&apos;t be impressed with technology until we can download food.
    </Typography>
  </footer>
)

Footer.propTypes = {
  classes: PropTypes.object,
}

export default withStyles(styles)(Footer)

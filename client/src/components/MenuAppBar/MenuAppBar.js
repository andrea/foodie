import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'

import { Link, NavLink, Redirect } from 'react-router-dom'
import { ApolloConsumer } from 'react-apollo'
// prettier-ignore
import { AppBar, Button, IconButton, Menu, MenuItem, Toolbar, Tooltip, Typography, withStyles } from '@material-ui/core'
import withWidth, { isWidthUp } from '@material-ui/core/withWidth'
// prettier-ignore
import { AccountCircle, Brightness4, Brightness5, Home, Menu as MenuIcon } from '@material-ui/icons'
import Toast from '../Toast/Toast'
import styles from './styles'
import { globals, routes } from '../../config'

class MenuAppBar extends React.Component {
  state = {
    auth: false,
    anchorEl: null,
    showToast: false,
    error: {},
  }

  componentDidMount() {
    const { session } = this.props
    if (session && session.getCurrentUser) {
      this.setState(() => ({ auth: session.getCurrentUser }))
    }
  }

  // noinspection JSCheckFunctionSignatures
  componentDidUpdate(prevProps) {
    if (prevProps.session !== this.props.session) {
      this.setState(() => ({ auth: this.props.session.getCurrentUser }))
    }
  }

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  handleClose = () => {
    this.setState({ anchorEl: null })
  }

  handleLogout = client => () => {
    localStorage.removeItem('token')
    client.resetStore()
    this.setState({ anchorEl: null })
  }

  handleShowToast = error => {
    this.setState({ showToast: true, error })
  }

  handleHideToast = (event, reason) => {
    if (reason === 'clickaway') return
    this.setState({ showToast: false })
  }

  // Static Links to avoid property collisions
  // More here => https://material-ui.com/demos/buttons/#third-party-routing-library
  // And here => https://material-ui.com/guides/composition/#caveat-with-inlining
  loginLink = itemProps => (
    <NavLink
      exact
      to={routes.login.path}
      activeClassName={this.props.classes.active}
      {...itemProps}
    />
  )

  render() {
    // prettier-ignore
    const { classes, toggleDrawer, togglePaletteType, paletteType } = this.props
    const { auth, anchorEl } = this.state
    const { appName } = globals
    const open = Boolean(anchorEl)
    const drawer = isWidthUp('sm', this.props.width) ? 'left' : 'top'

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              className={classes.menuButton}
              color="inherit"
              aria-label="Menu"
              onClick={toggleDrawer(drawer, true)}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              variant="title"
              color="inherit"
              className={classes.grow}
            >
              {appName}
            </Typography>
            <Tooltip title={'Home'}>
              <IconButton
                component={Link}
                to={routes.home.path}
                color="inherit"
              >
                <Home />
              </IconButton>
            </Tooltip>
            {auth ? (
              <div>
                <Tooltip
                  title={paletteType === 'light' ? 'Dark Theme' : 'Light Theme'}
                >
                  <IconButton onClick={togglePaletteType} color="inherit">
                    {paletteType === 'light' ? (
                      <Brightness4 />
                    ) : (
                      <Brightness5 />
                    )}
                  </IconButton>
                </Tooltip>
                <IconButton
                  aria-owns={open ? 'menu-appbar' : null}
                  aria-haspopup="true"
                  onClick={this.handleMenu}
                  color="inherit"
                  auth={auth}
                >
                  <AccountCircle />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={open}
                  onClose={this.handleClose}
                >
                  <MenuItem disabled divider>
                    Hello, {auth.username}!
                  </MenuItem>
                  <MenuItem
                    component={NavLink}
                    to="/profile"
                    onClick={this.handleClose}
                  >
                    Profile
                  </MenuItem>
                  <ApolloConsumer onError={this.handleShowToast}>
                    {(client, data) => {
                      if (data) {
                        return <Redirect to="/" />
                      }
                      return (
                        <Fragment>
                          <MenuItem onClick={this.handleLogout(client)}>
                            Logout
                          </MenuItem>
                          <Toast
                            close={this.handleHideToast}
                            message={this.state.error.message}
                            open={this.state.showToast}
                          />
                        </Fragment>
                      )
                    }}
                  </ApolloConsumer>
                </Menu>
              </div>
            ) : (
              <Tooltip title="Sign in to your account" enterDelay={300}>
                <div>
                  <Button color="inherit" component={this.loginLink}>
                    Login
                  </Button>
                </div>
              </Tooltip>
            )}
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}

MenuAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
  toggleDrawer: PropTypes.func.isRequired,
}

export default compose(
  withWidth(),
  withStyles(styles)
)(MenuAppBar)

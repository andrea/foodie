import React from 'react'
import PropTypes from 'prop-types'

const Logo = ({ fill, font, fontSize, ...rest }) => (
  <svg viewBox="0 0 250 250" {...rest}>
    <g fill={fill} fillRule="evenodd">
      <text
        fontFamily={font}
        fontSize={fontSize}
        fontWeight="bold"
        letterSpacing={0.376}
        transform="translate(65 35)"
      >
        <tspan x={0.299} y={173}>
          Foody
        </tspan>
      </text>
      <path d="M144 69v47a3.001 3.001 0 0 1-6 0V97.124l-2.656-2.574A5.416 5.416 0 0 1 134 90.98V87.03c0-8.895 5.128-17.214 6.141-18.77A2 2 0 0 1 144 69zm-16.021 86c.579-.496 1.042-1.177 1.3-2.045 1.23-4.11 4.275-11.347 7.965-14.586 3.618-3.169 32.45-16.766 32.45-45.174 0-23.856-19.856-43.195-44.347-43.195S81 69.34 81 93.195c0 28.408 28.831 42.005 32.444 45.174 3.69 3.233 6.735 10.476 7.965 14.586.26.867.725 1.549 1.305 2.045H95c-16.569 0-30-13.431-30-30V65c0-16.569 13.431-30 30-30h60c16.569 0 30 13.431 30 30v60c0 16.569-13.431 30-30 30h-27.021zm-5.805-87.11C122.658 70.426 124 77.724 124 81c0 8-6 10-6 10v25a3.001 3.001 0 0 1-6 0V91s-6-2-6-10c0-3.276 1.342-10.574 1.826-13.11a1.097 1.097 0 0 1 2.174.206V79.71a1.29 1.29 0 0 0 2.572.144l1.32-11.878c.062-.556.53-.976 1.09-.976h.036c.56 0 1.028.42 1.09.974l1.32 11.878A1.289 1.289 0 0 0 120 79.71V68.096a1.097 1.097 0 1 1 2.174-.206z" />
    </g>
  </svg>
)

Logo.propTypes = {
  fill: PropTypes.string.isRequired,
  font: PropTypes.string.isRequired,
  fontSize: PropTypes.number.isRequired,
}

Logo.defaultProps = {
  fill: '#651FFF',
  font:
    "'Ubuntu' 'system-ui' 'BlinkMacSystemFont' '-apple-system' 'Segoe UI' 'Roboto' 'Oxygen' 'Cantarell' 'Fira Sans' 'Droid Sans' 'sans-serif'",
  fontSize: 40,
}

export default Logo

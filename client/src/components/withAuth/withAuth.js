import React from 'react'
import { Redirect } from 'react-router-dom'
import { Query } from 'react-apollo'
import { getCurrentUser } from '../../graphql/queries'
import { routes } from '../../config'

const withAuth = conditionFunc => Component =>
  function ComponentWithAuth(props) {
    return (
      <Query query={getCurrentUser}>
        {({ data, loading }) => {
          if (loading) return null
          return conditionFunc(data) ? (
            <Component {...props} />
          ) : (
            <Redirect to={routes.home.path} />
          )
        }}
      </Query>
    )
  }

export default withAuth

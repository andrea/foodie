/* eslint-disable no-underscore-dangle */
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Query } from 'react-apollo'
import { compose } from 'recompose'
// prettier-ignore
import { Avatar, Button, Divider, IconButton, List, ListItem, ListItemSecondaryAction, ListItemText, Typography, withMobileDialog, withStyles } from '@material-ui/core'
import { Edit, Delete, Restaurant } from '@material-ui/icons'
import DeleteRecipe from '../DeleteRecipe/DeleteRecipe'
import EditRecipe from '../EditRecipe/EditRecipe'
import Toast from '../Toast/Toast'
// prettier-ignore
import { getUserRecipes, } from '../../graphql/queries'
import styles from './styles'

class UserRecipes extends Component {
  state = {
    modal: {
      delete: false,
      edit: false,
    },
    recipe: {},
    showToast: false,
    error: {},
  }

  handleClickOpen = (modal, recipe) => {
    this.setState(prevState => ({
      modal: { ...prevState.modal, [modal]: true },
      recipe,
    }))
  }

  handleClose = modal => {
    this.setState(prevState => ({
      modal: { ...prevState.modal, [modal]: false },
    }))
  }

  handleDelete = deleteUserRecipeMutation => {
    deleteUserRecipeMutation().then(() => {
      this.setState(prevState => ({
        modal: { ...prevState.modal, delete: false },
      }))
    })
  }

  handleUpdate = (event, updateUserRecipeMutation) => {
    event.preventDefault()
    updateUserRecipeMutation().then(() => {
      this.setState(prevState => ({
        modal: { ...prevState.modal, edit: false },
      }))
    })
  }

  handleShowToast = error => {
    this.setState({ showToast: true, error })
  }

  handleHideToast = (event, reason) => {
    if (reason === 'clickaway') return
    this.setState({ showToast: false })
  }

  render() {
    const { username, classes, fullScreen } = this.props

    return (
      <Fragment>
        <Query
          query={getUserRecipes}
          variables={{ username }}
          onError={this.handleShowToast}
        >
          {({ data, loading }) => {
            if (loading) return <Typography variant="body1">Loading</Typography>
            if (data.getUserRecipes.length < 1) {
              return (
                <Button
                  disabled
                  variant="outlined"
                  size="large"
                  fullWidth
                  className={classes.button}
                >
                  You do not have any recipes yet.
                </Button>
              )
            }

            return (
              <div className={classes.root}>
                <List component="nav">
                  {data.getUserRecipes.map(recipe => (
                    <Fragment key={recipe._id}>
                      <ListItem
                        button
                        component={Link}
                        to={`/recipes/${recipe._id}`}
                      >
                        <Avatar>
                          <Restaurant />
                        </Avatar>
                        <ListItemText
                          classes={{ secondary: classes.secondary }}
                          primary={recipe.name}
                          secondary={`${recipe.likes} Likes`}
                        />
                        <ListItemSecondaryAction>
                          <IconButton aria-label="Edit Recipe">
                            <Edit
                              onClick={() =>
                                this.handleClickOpen('edit', recipe)
                              }
                            />
                          </IconButton>
                          <IconButton aria-label="Delete Recipe">
                            <Delete
                              onClick={() =>
                                this.handleClickOpen('delete', recipe)
                              }
                            />
                          </IconButton>
                        </ListItemSecondaryAction>
                      </ListItem>
                      <Divider />
                    </Fragment>
                  ))}
                </List>
              </div>
            )
          }}
        </Query>
        <EditRecipe
          fullScreen={fullScreen}
          open={this.state.modal.edit}
          handleUpdate={this.handleUpdate}
          handleOnError={this.handleShowToast}
          handleClose={this.handleClose}
          recipe={this.state.recipe}
          username={username}
        />
        <DeleteRecipe
          fullScreen={fullScreen}
          open={this.state.modal.delete}
          handleDelete={this.handleDelete}
          handleOnError={this.handleShowToast}
          handleClose={this.handleClose}
          recipe={this.state.recipe}
          username={username}
        />
        <Toast
          close={this.handleHideToast}
          message={this.state.error.message}
          open={this.state.showToast}
        />
      </Fragment>
    )
  }
}

UserRecipes.propTypes = {
  classes: PropTypes.object,
  fullScreen: PropTypes.bool,
  username: PropTypes.string,
}

export default compose(
  withStyles(styles),
  withMobileDialog()
)(UserRecipes)

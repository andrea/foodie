export default theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4,
  },
  secondary: {
    fontSize: theme.typography.pxToRem(12),
  },
  button: {
    textTransform: 'none',
  },
})

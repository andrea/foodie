import { blue, deepPurple, red } from '@material-ui/core/colors'

export const defaultOptions = {
  typography: {
    // Tell Material-UI what's the font-size on the html element is.
    htmlFontSize: 10,
    // The default font value is 14px.
    fontSize: 10,
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      'Ubuntu',
      'system-ui',
      'BlinkMacSystemFont',
      '-apple-system',
      'Segoe UI',
      'Roboto',
      'Oxygen',
      'Ubuntu',
      'Cantarell',
      'Fira Sans',
      'Droid Sans',
      'Helvetica Neue',
      'sans-serif',
    ].join(','),
  },
  palette: {
    primary: {
      light: deepPurple.A200,
      main: deepPurple.A400,
      dark: deepPurple.A700,
    },
    secondary: {
      light: blue.A200,
      main: blue.A400,
      dark: blue.A700,
    },
    error: {
      light: red.A200,
      main: red.A400,
      dark: red.A700,
    },
    // Used by `getContrastText()` to maximize the contrast between the background and
    // the text.
    contrastThreshold: 3,
    // Used to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.2,
  },
  shape: {
    borderRadius: 2,
  },
}

export const defaultPalette = {
  palette: {
    primary: {
      light: deepPurple.A200,
      main: deepPurple.A400,
      dark: deepPurple.A700,
    },
    secondary: {
      light: blue.A200,
      main: blue.A400,
      dark: blue.A700,
    },
    error: {
      light: red.A200,
      main: red.A400,
      dark: red.A700,
    },
    // Used by `getContrastText()` to maximize the contrast between the background and
    // the text.
    contrastThreshold: 3,
    // Used to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.2,
  },
}

import Recipes from './components/Recipes/Recipes'
import Login from './components/Login/Login'
import Register from './components/Register/Register'
import AddRecipe from './components/AddRecipe/AddRecipe'
import Search from './components/Search/Search'
import Profile from './components/Profile/Profile'
import RecipePage from './components/RecipePage/RecipePage'

export const globals = {
  appName: 'Foodie',
  appVersion: '1.0.0',
}

export const routes = {
  home: {
    path: '/',
    component: Recipes,
  },
  login: {
    path: '/login',
    component: Login,
  },
  register: {
    path: '/register',
    component: Register,
  },
  recipe: {
    path: '/recipes:_id',
    component: RecipePage,
  },
  addRecipe: {
    path: '/recipe/add',
    component: AddRecipe,
  },
  search: {
    path: '/search',
    component: Search,
  },
  profile: {
    path: '/profile',
    component: Profile,
  },
}

export const formatDate = (timestamp, dateAndTime) => {
  const newDate = new Date(Date(timestamp))
  const dateOptions = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  }
  const timeOptions = {
    hour: 'numeric',
    minute: 'numeric',
  }

  const date = new Intl.DateTimeFormat('en-US', dateOptions).format(newDate)
  const time = new Intl.DateTimeFormat('en-US', timeOptions).format(newDate)

  if (dateAndTime) {
    return `${date} at ${time}`
  }
  return date
}

export const textTruncate = (str, length = 84, ending = '...') => {
  if (str.length > length) {
    return str.substring(0, length - ending.length) + ending
  }
  return str
}

export const getWelcomeText = () => {
  const date = new Date()
  const hr = date.getHours()
  let welcomeText
  if (hr > 5 && hr < 11) {
    welcomeText = 'What would you like for breakfast?'
  } else if (hr > 12 && hr < 15) {
    welcomeText = 'What would you like for lunch?'
  } else if (hr > 18 && hr < 22) {
    welcomeText = 'What would you like for dinner?'
  } else {
    welcomeText = 'Would you like a snack?'
  }
  return welcomeText
}

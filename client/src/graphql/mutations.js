import { gql } from 'apollo-boost'
import recipeFragments from './fragments'

export const registerUser = gql`
  mutation($username: String!, $email: String!, $password: String!) {
    registerUser(username: $username, email: $email, password: $password) {
      token
    }
  }
`

export const loginUser = gql`
  mutation($username: String!, $password: String!) {
    loginUser(username: $username, password: $password) {
      token
    }
  }
`

export const addRecipe = gql`
  mutation(
    $name: String!
    $description: String!
    $category: String!
    $imageUrl: String!
    $instructions: String!
    $username: String
  ) {
    addRecipe(
      name: $name
      description: $description
      category: $category
      imageUrl: $imageUrl
      instructions: $instructions
      username: $username
    ) {
      ...CompleteRecipe
    }
  }
  ${recipeFragments.recipe}
`

export const deleteUserRecipe = gql`
  mutation($_id: ObjectID!) {
    deleteUserRecipe(_id: $_id) {
      _id
    }
  }
`

export const updateUserRecipe = gql`
  mutation(
    $_id: ObjectID!
    $name: String!
    $description: String!
    $instructions: String!
    $category: String!
    $imageUrl: String!
  ) {
    updateUserRecipe(
      _id: $_id
      name: $name
      description: $description
      instructions: $instructions
      category: $category
      imageUrl: $imageUrl
    ) {
      _id
      name
      imageUrl
      category
      description
      instructions
      likes
    }
  }
`

export const likeRecipe = gql`
  mutation($_id: ObjectID!, $username: String!) {
    likeRecipe(_id: $_id, username: $username) {
      ...LikeRecipe
    }
  }
  ${recipeFragments.like}
`

export const unlikeRecipe = gql`
  mutation($_id: ObjectID!, $username: String!) {
    unlikeRecipe(_id: $_id, username: $username) {
      ...LikeRecipe
    }
  }
  ${recipeFragments.like}
`

import { gql } from 'apollo-boost'
import recipeFragments from './fragments'

export const getAllRecipes = gql`
  query {
    getAllRecipes {
      _id
      name
      imageUrl
      description
      category
      likes
      createdAt
    }
  }
`

export const getRecipe = gql`
  query($_id: ObjectID!) {
    getRecipe(_id: $_id) {
      ...CompleteRecipe
    }
  }
  ${recipeFragments.recipe}
`

export const searchRecipes = gql`
  query($searchTerm: String) {
    searchRecipes(searchTerm: $searchTerm) {
      _id
      name
      category
      imageUrl
      description
      likes
    }
  }
`

export const getCurrentUser = gql`
  query {
    getCurrentUser {
      username
      joinedAt
      email
      favorites {
        _id
        name
      }
    }
  }
`

export const getUserRecipes = gql`
  query($username: String!) {
    getUserRecipes(username: $username) {
      _id
      name
      category
      imageUrl
      description
      instructions
      createdAt
      likes
    }
  }
`

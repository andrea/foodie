import { gql } from 'apollo-boost'

export default {
  recipe: gql`
    fragment CompleteRecipe on Recipe {
      _id
      name
      description
      category
      imageUrl
      instructions
      createdAt
      likes
      username
    }
  `,
  like: gql`
    fragment LikeRecipe on Recipe {
      _id
      likes
    }
  `,
}

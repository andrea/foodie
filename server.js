/* eslint-disable no-console */
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const jwt = require('jsonwebtoken')

// Bring in GraphQL-Express middleware
const { ApolloServer } = require('apollo-server-express')
require('dotenv').config()

// Mongoose models
const Recipe = require('./models/Recipe')
const User = require('./models/User')

// GraphQL
const { typeDefs } = require('./schema')
const { resolvers } = require('./resolvers')

const PORT = process.env.PORT || 4444

// Connect to MongoDB instance
// prettier-ignore
mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true }
  )
  .then(() => console.log('DB Connected'))
  .catch(err => console.error(err))
mongoose.set('useCreateIndex', true) // Get rid of (node:50897) DeprecationWarning: collection.ensureIndex is deprecated. Use createIndexes instead.

// Initialize the application
const app = express()

// CORS
const corsOptions = {
  origin: 'http://localhost:3000',
  credentials: true,
}

app.use(cors(corsOptions))

// Set up JWT authentication middleware
app.use((req, res, next) => {
  const token = req.headers.authorization
  // eslint-disable-next-line security/detect-possible-timing-attacks
  if (token !== 'null') {
    try {
      req.currentUser = jwt.verify(token, process.env.SECRET)
    } catch (err) {
      console.error(err)
    }
  }
  next()
})

// Create Apollo server
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req }) => ({
    Recipe,
    User,
    currentUser: req.currentUser,
  }),
})

server.applyMiddleware({ app })

app.listen(PORT, () => {
  console.log(`🚀 Server listening on ${PORT}${server.graphqlPath}.`)
})

const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const { Schema } = mongoose

const user = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  joinedAt: {
    type: Date,
    default: Date.now,
  },
  favorites: {
    type: [Schema.Types.ObjectId],
    ref: 'Recipe',
  },
})

user.pre('save', function hashPassword(next) {
  if (!this.isModified('password')) return next()

  return bcrypt
    .hash(this.password, 10)
    .then(hash => {
      this.password = hash
      return next()
    })
    .catch(next)
})

module.exports = mongoose.model('User', user)

const mongoose = require('mongoose')

const { Schema } = mongoose

const recipe = new Schema({
  name: {
    type: String,
    required: true,
  },
  imageUrl: {
    type: String,
    required: true,
  },
  category: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  instructions: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  likes: {
    type: Number,
    default: 0,
  },
  username: {
    type: String,
  },
})

// Not a fan of kitchen-sinky indexes
// recipe.index({
//   '$**': 'text',
// })

recipe.index(
  {
    name: 'text',
    description: 'text',
    instructions: 'text',
  },
  {
    weights: {
      name: 10,
      description: 8,
      instructions: 4,
    },
  }
)

module.exports = mongoose.model('Recipe', recipe)

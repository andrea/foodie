# ClientPanel

A React/Node recipe management application, featuring:

- Full CRUD operations on recipe records
- Recipe liking
- User profiles tracking own recipes and recipe likes
- User authentication


## Requirements

To run the application you will need to:

- [Install Node.js][Install Node] runtime environment
- create a MongoDB database


## Quick Start

To run the application...

- Clone the repository
- Set up a MongoDB database. Both [mLab][mLab] and [mongoDB][mongoDB] offer free plans to get started quickly.
- Open Terminal, head over the project root folder, then rename the file `example.env` to `.env`
- Edit you `.env` file and enter your database connection string.
- From the project's root folder, install the application by running:  

```bash
npm install
```

- Start the client application by running:
  
```bash
npm start
```

Now head to `http://localhost:3000` and see your running app!

[Install Node]: https://nodejs.org/en/download/
[mLab]: https://mlab.com
[mongoDB]: https://www.mongodb.com/cloud/atlas
